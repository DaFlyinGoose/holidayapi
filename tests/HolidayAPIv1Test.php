<?php

/**
 * Class HolidayAPIv1Test
 * Testing HolidayAPIv1, each test has to run in seperate process due to headers being set through the test, not the
 * best solution overhead wise, but given the nature of this test I think its the most appropriate solution at this point.
 */
class HolidayAPIv1Test extends TestCase
{
    protected $holidayAPI;
    // Must have a data value with holidays in.
    protected $country = 'GB';
    // These stats must reflect the country file set above (currently GB)
    protected $countryStats = [
        'holidays' => 12,
        'holidaysInJan' => 1,
        'holidaysInDec' => 2,
        'holiday' => [
            // Should reference a specific date holiday which wont overlap with other holidays
            'date' => ['month' => 1, 'day' => 1],
            'name' => "New Year's Day",
            'previousHolidayName' => "Boxing Day (possibly in lieu)",
            'upcomingHolidayName' => "Valentine's Day",
        ],
    ];

    public function setUp()
    {
        parent::setUp();

        // Injecting the holiday service via Laravels IoC Container
        $this->holidayAPI = app('App\Http\Services\HolidayService');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetHoliday() {
        $data = $this->getHolidayData();
        $this->assertEquals($this->countryStats['holidays'], $this->countHolidays($data), 'More or less holidays were returned than expected.');

        $data = $this->getHolidayData(['year' => 2100]);
        $this->assertEquals($this->countryStats['holidays'], $this->countHolidays($data), 'More or less holidays were returned than expected when testing year 2100.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetHolidayWithMonth() {
        $data = $this->getHolidayData(['month' => 1]);
        $this->assertEquals($this->countryStats['holidaysInJan'], $this->countHolidays($data, true), 'More or less Jan holidays were returned than expected.');

        $data = $this->getHolidayData(['month' => 12]);
        $this->assertEquals($this->countryStats['holidaysInDec'], $this->countHolidays($data, true), 'More or less Dec holidays were returned than expected.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetHolidayWithDay() {
        $data = $this->getHolidayData($this->countryStats['holiday']['date']);
        $this->assertCount(1, $data['holidays'], 'Multiple holidays occured on configured single holiday date.');
        $this->assertEquals($this->countryStats['holiday']['name'], $data['holidays'][0]['name'], 'Specific Day Holiday name did not match the expected.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetPreviousHolidayWithDay() {
        $data = $this->getHolidayData(array_merge($this->countryStats['holiday']['date'], ['previous' => true]));
        $this->assertCount(1, $data['holidays'], 'Multiple holidays occured on configured single holiday date.');
        $this->assertEquals($this->countryStats['holiday']['previousHolidayName'], $data['holidays'][0]['name'], 'Specific Day Holiday name did not match the expected.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetUpcomingHolidayWithDay() {
        $data = $this->getHolidayData(array_merge($this->countryStats['holiday']['date'], ['upcoming' => true]));
        $this->assertCount(1, $data['holidays'], 'Multiple holidays occured on configured single holiday date.');
        $this->assertEquals($this->countryStats['holiday']['upcomingHolidayName'], $data['holidays'][0]['name'], 'Specific Day Holiday name did not match the expected.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetHolidayWithoutYear() {
        $data = $this->getHolidayData(['year' => ''], 400);
        $this->assertEquals('The year parameter is required.', $data['error'], 'Unexpected error message returned for invalid case.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetHolidayWithoutCountry() {
        $data = $this->getHolidayData(['country' => ''], 400);
        $this->assertEquals('The country parameter is required.', $data['error'], 'Unexpected error message returned for invalid case.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetHolidayWithInvalidCountry() {
        $data = $this->getHolidayData(['country' => 'KP'], 400);
        $this->assertEquals("The supplied country (KP) is not supported at this time.", $data['error'], 'Unexpected error message returned from North Korea.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetPreviousHolidayWithDayWithoutMonth() {
        $data = $this->getHolidayData(['day' => 1, 'previous' => true], 400);
        $this->assertEquals("The month parameter is required when requesting previous holidays.", $data['error'], 'Unexpected error message returned for invalid case.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetHolidayWithMonthWithoutDay() {
        $data = $this->getHolidayData(['month' => 1, 'upcoming' => true], 400);
        $this->assertEquals("The day parameter is required when requesting upcoming holidays.", $data['error'], 'Unexpected error message returned for invalid case.');
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetPreviousHolidayWithMonthWithoutDay() {
        $data = $this->getHolidayData(['month' => 1, 'previous' => true], 400);
        $this->assertEquals("The day parameter is required when requesting previous holidays.", $data['error'], 'Unexpected error message returned for invalid case.');
    }

    /**
     * I don't like this behaviour but in the interest of feature parity I have left it in...
     *
     * @runInSeparateProcess
     */
    public function testGetHolidayWithDayWithoutMonth() {
        $data = $this->getHolidayData(['day' => 1]);
        $this->assertEquals($this->countryStats['holidays'], $this->countHolidays($data), 'More or less holidays were returned than expected.');
    }



    /**
     * Count how many holidays have been returned
     *
     * @param array $data - API holiday data
     * @param bool $month - used to determine the count method
     * @return int
     * @throws Exception
     */
    protected function countHolidays(array $data, $month = false) {
        if (!isset($data['holidays'])) {
            throw new \Exception('No holidays found in returned data.');
        }

        // Annoyingly data from holidays over a month differ slightly in structure to other requests
        if ($month) {
            return count($data['holidays']);
        }

        $holidayCount = 0;
        foreach ($data['holidays'] as $holiday) {
            $holidayCount += count($holiday);
        }

        return $holidayCount;
    }

    /**
     * Gets holiday data from the API, accepting different inputs and making the obvious checks around available
     * data.
     *
     * @param array $inputs
     * @param int $expectedStatus
     * @param bool $expectingHolidays
     * @return mixed
     * @throws Exception
     */
    protected function getHolidayData(array $inputs = [], $expectedStatus = 200, $expectingHolidays = true) {
        $input = array_merge(['country' => $this->country, 'year' => 2017], $inputs);

        $data = $this->holidayAPI->getHolidays($input);

        if (!isset($data['status'])) {
            throw new \Exception('No status returned from API.');
        }
        elseif ($data['status'] != $expectedStatus) {
            dd($data);
            throw new \Exception('Unexpected status returned from API.');
        }

        if ($data['status'] == 400 && !isset($data['error'])) {
            throw new \Exception('No error message returned from failed API request');
        }

        if ($expectedStatus == 200 && $expectingHolidays && !isset($data['holidays'])) {
            throw new \Exception('No holidays found in returned data.');
        }

        return $data;
    }
}