<?php
namespace App\Http\Services;

use App\Holiday;
use Illuminate\Validation\Factory as Validator;

class HolidayService
{
    private $cache = false;

    private $inputDefaults = [
        'country'   => '',
        'year'      => '',
        'month'     => '',
        'day'       => '',
        'date'      => '',
        'previous'  => null,
        'upcoming'  => null,
        'official'  => null,
    ];

    private $validator;
    private $validationRules = [
        'country'   => 'required|exists:holidays,country',
        'year'      => 'required',
        'month'     => 'required_with:previous,upcoming',
        'day'       => 'required_with:previous,upcoming',
        'date'      => 'required_with_all:month,day|date',
    ];

    public function __construct(Validator $validator, $cache = false)
    {
        $this->validator = $validator;

        if (config('app.env') != 'local') {
            $this->cache = $cache;
        }
    }

    /**
     * This method will return an array of holidays depending on what $inputs it receives.
     *
     * $input must contain: country and year, optional keys include: month, day, previous, upcoming and official.
     *
     * Example $input array:
     * ['country' => 'GB',
     *  'year' => '2017',
     *  'month' => 1,
     *  'day' => 1,
     *  'upcoming' => true,
     *  'previous' => false,
     *  'official' => true,
     * ]
     *
     * Note this function will throw an exception if both upcoming AND previous are true
     *
     * @param array $input
     * @return array
     */
    public function getHolidays(array $input)
    {
        if (ini_get('date.timezone') == '') {
            date_default_timezone_set('UTC');
        }

        // Create the standard payload that will be returns, this will be filled further down
        $payload  = ['status' => 200];

        try {
            // Bit of magic, since the input is being cleaned and validated we can trust its ok to use extract.
            $data = $this->validatateData($this->cleanData($input));

            $country_holidays = $this->calculateHolidays($data);
        } catch (\Exception $e) {
            // Fill the payload with error information if an exception has been thrown
            $payload['status'] = 400;
            $payload['error']  = $e->getMessage();

            return $payload;
        }

        return array_merge($payload, ['holidays' => $this->formatHolidays($country_holidays, $data)]);
    }

    /**
     * Formats the found holidays depending on what inputs was specified
     *
     * @param array $countryHolidays
     * @param array $data
     * @return array|mixed|null
     */
    private function formatHolidays($countryHolidays, $data)
    {
        $holidays = [];

        if ($data['date']) {
            // Returns the last holiday
            if ($data['previous']) {
                $countryHolidays = $this->flatten($data['date'], $countryHolidays[$data['year'] - 1], $countryHolidays[$data['year']]);
                prev($countryHolidays);

                return current($countryHolidays);
            }
            // Returns the next holiday
            elseif ($data['upcoming']) {
                $countryHolidays = $this->flatten($data['date'], $countryHolidays[$data['year']], $countryHolidays[$data['year'] + 1]);
                next($countryHolidays);

                return current($countryHolidays);
            }
            // Returns any holidays from specified date
            elseif (isset($countryHolidays[$data['year']][$data['date']])) {
                return $countryHolidays[$data['year']][$data['date']];
            }

            // Would love to put an exception here, but technically that would change existing behaviour
            return null;
        }

        // Returns all holidays from a given month/year
        if ($data['month']) {
            foreach ($countryHolidays[$data['year']] as $date => $holiday) {
                if (substr($date, 0, 7) == $data['year'] . '-' . $data['month']) {
                    $holidays = array_merge($holidays, $holiday);
                }
            }

            return $holidays;
        }

        // Returns all holidays from a given year
        return $countryHolidays[$data['year']];
    }

    /**
     * Creates an array containing all holidays over 1 or more years in an array structured below:
     *
     * ['2017' => [
     *      '2017-05-04' => [
     *          ['name' => 'Star Wars Day',
     *           'country' => 'US',
     *           'date' => '2017-05-04',
     *          ],
     *      ],
     * ]
     *
     * @param string $country - 2 letter representation of a country
     * @param string $year - year to fetch holidays for
     * @param bool $range - whether to include a year either side
     * @param bool $official - whether to only show official holidays
     * @return array
     * @throws \Exception
     */
    private function calculateHolidays($data)
    {
        $country = $data['country'];
        $year = $data['year'];
        $range = ($data['previous'] || $data['upcoming']);
        $official = $data['official'];
        $return = [];

        // Establishes the range of years to return holidays for and loops through them
        if ($range) {
            $years = [$year - 1, $year, $year + 1];
        } else {
            $years = [$year];
        }

        // Generates all holidays by year, results in an array with year keys
        foreach ($years as $year) {
            // Checks the cache to see if we already have the data
            if ($this->cache) {
                $cache_key        = 'holidayapi:' . $country . ':holidays:' . $year;
                $country_holidays = $this->cache->get($cache_key);
            } else {
                $country_holidays = false;
            }

            if ($country_holidays) {
                $country_holidays = unserialize($country_holidays);
            } else {
                $holidays = Holiday::where('country', $country);

                if ($official) {
                    $holidays->where('official', $official);
                    if (!$holidays->count()) {
                        throw new \Exception('No official holidays found for ' . $country . ' at this time.');
                    }
                }

                // Return country holiday data as an associative array
                $country_holidays    = $holidays->get()->toArray();
                $calculated_holidays = [];

                foreach ($country_holidays as $country_holiday) {
                    // Replaces placeholders in rules, the if statement below results in the rule always having a year
                    // added, whether at the end of the string or in a placeholder.
                    if (strstr($country_holiday['rule'], '%Y')) {
                        $rule = str_replace('%Y', $year, $country_holiday['rule']);
                    } elseif (strstr($country_holiday['rule'], '%EASTER')) {
                        // Establish when easter is in a year
                        $rule = str_replace('%EASTER', date('Y-m-d', strtotime($year . '-03-21 +' . easter_days($year) . ' days')), $country_holiday['rule']);
                    } elseif (in_array($country, ['BR', 'US']) && strstr($country_holiday['rule'], '%ELECTION')) {
                        // If an election is occuring the same year as we are looking at we update the rule, otherwise
                        // we mark the rule as false.
                        switch ($country) {
                            case 'BR':
                                $years = range(2014, $year, 2);
                                break;
                            case 'US':
                                $years = range(1788, $year, 4);
                                break;
                        }

                        if (in_array($year, $years)) {
                            $rule = str_replace('%ELECTION', $year, $country_holiday['rule']);
                        } else {
                            $rule = false;
                        }
                    } else {
                        $rule = $country_holiday['rule'] . ' ' . $year;
                    }

                    // Check the rule isn't false (can only happen due to elections)
                    if ($rule) {
                        $calculated_date = date('Y-m-d', strtotime($rule));

                        if (!isset($calculated_holidays[$calculated_date])) {
                            $calculated_holidays[$calculated_date] = [];
                        }

                        // Add a new holiday on a specific date
                        $calculated_holidays[$calculated_date][] = [
                            'name'    => $country_holiday['name'],
                            'country' => $country,
                            'date'    => $calculated_date,
                        ];
                    }
                }

                $country_holidays = $calculated_holidays;

                // Sort by key, in this case a date string (YYYY-MM-DD)
                ksort($country_holidays);

                foreach ($country_holidays as $date_key => $date_holidays) {
                    // Orders holidays alphabetically by their name
                    usort($date_holidays, function($a, $b)
                    {
                        $a = $a['name'];
                        $b = $b['name'];

                        if ($a == $b) {
                            return 0;
                        }

                        return $a < $b ? -1 : 1;
                    });

                    $country_holidays[$date_key] = $date_holidays;
                }

                if ($this->cache) {
                    $this->cache->setex($cache_key, 3600, serialize($country_holidays));
                }
            }

            $return[$year] = $country_holidays;
        }

        return $return;
    }

    /**
     * Merges two arrays and set the resulting arrays internal pointer to a date representing key.
     *
     * It's assumed both arrays passed contain date keys, and that the date passed will be the
     * current date, allowing us to see previous holidays with prev() and future holidays with next().
     *
     * Warning - This method will override data in the first array if the same key exists in the second array.
     *
     * @param string $date - meant to be current date
     * @param array $array1 - array using keys representing dates
     * @param array $array2 - array using keys representing dates
     * @return array
     */
    private function flatten($date, $array1, $array2)
    {
        $holidays = array_merge($array1, $array2);

        // Injects a date as a placeholder if that date doesn't exist
        if (!isset($holidays[$date])) {
            $holidays[$date] = false;
            ksort($holidays);
        }

        // Sets the internal pointer to date specified
        while (key($holidays) !== $date) {
            next($holidays);
        }

        return $holidays;
    }

    /**
     * Remove any extra items from the array, merge defaults and clean/set month and date
     *
     * @param $data
     * @return array
     */
    private function cleanData($data)
    {
        // Strip any unwanted data and set defaults for missing parameters
        $data = array_only($data, array_keys($this->inputDefaults));
        $data = array_merge($this->inputDefaults, $data);

        $data['country'] = strtoupper($data['country']);

        // Add leading 0's to the month
        if ($data['month'] != '') {
            $data['month'] = str_pad($data['month'], 2, '0', STR_PAD_LEFT);
        }

        // Set the date that will only be used if both month and day is set
        if ($data['month'] && $data['day']) {
            // We add leading 0's to day here as this is the only place its needed
            $data['date'] = $data['year'] . '-' . $data['month'] . '-' . str_pad($data['day'], 2, '0', STR_PAD_LEFT);
        }

        return $data;
    }

    /**
     * Checks that all data passes validation rules and if not throws an exception to highlight the issue
     *
     * @param $data
     * @return array
     * @throws \Exception
     */
    private function validatateData($data)
    {
        // Due to variables being used in the error messages we have to set these here, be nice if Laravel Validator had
        // placeholders
        $messages = [
            'country.required' => 'The country parameter is required.',
            'country.exists' => 'The supplied country (' . $data['country'] . ') is not supported at this time.',
            'year.required' => 'The year parameter is required.',
            'month.required_with' => 'The month parameter is required when requesting ' . ($data['previous']? 'previous' : 'upcoming') . ' holidays.',
            'day.required_with' => 'The day parameter is required when requesting ' . ($data['previous']? 'previous' : 'upcoming') . ' holidays.',
            'date.required_with' => 'The supplied date (' . $data['date'] . ') is invalid.',
            'date.date' => 'The supplied date (' . $data['date'] . ') is invalid.',
        ];

        $validator = $this->validator->make($data, $this->validationRules, $messages);

        if ($validator->fails()) {
            throw new \Exception($validator->errors()->first());
        }

        // Annoyingly you can't cover this check with Laravels validation rules
        if ($data['previous'] && $data['upcoming']) {
            throw new \Exception('You cannot request both previous and upcoming holidays.');
        }

        return $data;
    }
}