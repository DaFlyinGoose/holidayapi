<?php
namespace App\Http\Controllers;


use App\Http\Services\HolidayService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getIndex()
    {
        return view('home');
    }

    public function getHolidays(HolidayService $holidayService, Request $request)
    {
        $holidays = $holidayService->getHolidays($request->all());

        $status_message = $holidays['status'] . ' ' . ($holidays['status'] == 200 ? 'OK' : 'Bad Request');
        $headers = ['HTTP/1.1 ' . $status_message, 'Status: ' . $status_message];

        return JsonResponse::create($holidays, $holidays['status'], $headers);
    }
}