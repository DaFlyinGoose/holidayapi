<?php

namespace App\Providers;

use App\Http\Services\HolidayService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Http\Services\HolidayService', function ($app) {
            return new HolidayService($app->make('Illuminate\Validation\Factory'));
        });
    }
}
