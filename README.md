# Holiday API

Holiday API is a fork of joshtronic's (https://github.com/joshtronic) original code for obtaining information about holidays. 

## Local Development

To install `composer`:

```shell
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

To install the dependencies:

```shell
composer install
npm install
```

To install `gulp`:

```shell
npm install -g gulp
```

To run the server:

```shell
gulp server
```

To run PHPunit tests:
```shell
phpunit
```

To setup application:
*Run composer install
*If no .env file exists, copy .env.example and run php artisan key:generate in terminal
*Create a database called holidays (or change db name in your new .env file)
*Migrate new tables to database by running php artisan migrate in terminal
*Seed database with holidays by running php artisan db:seed in terminal