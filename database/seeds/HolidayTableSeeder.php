<?php

use Illuminate\Database\Seeder;

class HolidayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get a list of all country holiday files we have
        $path = database_path('seeds/countryHolidays');
        $files = scandir($path);
        foreach ($files as $fileName) {
            if (!in_array($fileName, ['.', '..'])) {
                $country = substr($fileName, 0, 2);
                // Eloquent expects an associative array when using create, so we'll use the json assoc flag
                $holidays = json_decode(file_get_contents($path . '/' . $fileName), true);
                foreach ($holidays as $holiday) {
                    \App\Holiday::create(array_merge(['country' => $country, 'official' => 1], $holiday));
                }
            }
        }
    }
}
